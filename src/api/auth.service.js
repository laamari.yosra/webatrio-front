import Vue from 'vue'

import config from "./config.json"

const url = config.url + "/auth"
export function register(user) {
    return Vue.http.post(url + "/register", user);
}

export function login(user) {
    return new Promise((resolve, reject) => {
        Vue.http.post(url + "/login", user).then((response) => {
            localStorage.token = response.body.data.token;
            resolve(response.body.data.user);

        }).catch(err => reject(err));
    });
}

export function getInfo(token) {
    return Vue.http.get(url + "/info", {
        headers: {
            Authorization: 'bearer ' + token
        }
    });
}