import vue from "vue";
import config from "./config.json"

const url = config.url + "/api/clients";

export function getAllClients() {
    return vue.http.get(url);
}

export function getClientById(id) {
    return vue.http.get(url + "/" + id);
}

export function addClient(client) {
    return vue.http.post(url, client)
}

export function editClient(id, client) {
    let aux = new Object();
    Object.assign(aux,client);
    delete aux._id;
    return vue.http.put(url + '/' + id, aux)
}

export function deleteClient(id) {
    return vue.http.delete(url + '/' + id);
}