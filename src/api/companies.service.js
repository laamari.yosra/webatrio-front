import vue from "vue";
import config from "./config.json"

const url = config.url + "/api/companies";

export function getAllCompanies() {
    return vue.http.get(url);
}

export function getCompanyById(id) {
    return vue.http.get(url + "/" + id);
}

export function addCompany(company) {
    return vue.http.post(url, company)
}

export function editCompany(id, company) {
    let aux = new Object();
    Object.assign(aux,copmany);
    delete aux._id;
    return vue.http.put(url + '/' + id, aux)
}

export function deleteCompany(id) {
    return vue.http.delete(url + '/' + id);
}