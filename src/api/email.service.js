import vue from 'vue';
let emailUrl = "https://redcarpet-thegate.herokuapp.com/emails";
let emailData = {
    // 'to': to,
    'from': "yosra.laamari@esprit.tn",
    // 'subject': subject,
    // 'content': content,
    'target': 'app',
    'username': 'yosra'
};

export function sendEmail(to, subject, content) {
    emailData.to = to;
    emailData.subject = subject;
    emailData.content = content;
    return vue.http.post(emailUrl, emailData);
}