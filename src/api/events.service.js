import vue from "vue";
import config from "./config.json"

const url = config.url + "/api/events";

export function getAllEvents() {
    return vue.http.get(url);
}

export function getEventById(id) {
    return vue.http.get(url + "/" + id);
}

export function addEvent(event) {
    return vue.http.post(url, event)
}

export function editEvent(id, event) {
    let aux = new Object();
    Object.assign(aux,event);
    delete aux._id;
    return vue.http.put(url + '/' + id, aux)
}

export function deleteEvent(id) {
    return vue.http.delete(url + '/' + id);
}