import vue from "vue";
import config from "./config.json"

const url = config.url + "/api/projects";

export function getAllProjects() {
    return vue.http.get(url);
}

export function getProjectById(id) {
    return vue.http.get(url + "/" + id);
}

export function addProject(project) {
    return vue.http.post(url, project)
}

export function editProject(id, project) {
    let aux = new Object();
    Object.assign(aux,project);
    delete aux._id;
    return vue.http.put(url + '/' + id, aux);
}

export function deleteProject(id) {
    return vue.http.delete(url + '/' + id);
}