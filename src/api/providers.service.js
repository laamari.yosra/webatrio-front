import vue from "vue";
import config from "./config.json"

const url = config.url + "/api/providers";

export function getAllProviders() {
    return vue.http.get(url);
}

export function getProviderById(id) {
    return vue.http.get(url + "/" + id);
}

export function addProvider(provider) {
    return vue.http.post(url, provider)
}

export function editProvider(id, provider) {
    let aux = new Object();
    Object.assign(aux,provider);
    delete aux._id;
    return vue.http.put(url + '/' + id, aux)
}

export function deleteProvider(id) {
    return vue.http.delete(url + '/' + id);
}