import vue from "vue";
import config from "./config.json"

const url = config.url + "/api/requests";

export function getAllRequests() {
    return vue.http.get(url);
}

export function getRequestById(id) {
    return vue.http.get(url + "/" + id);
}

export function addRequest(request) {
    return vue.http.post(url, request);
}

export function editRequest(id, request) {
    let aux = new Object();
    Object.assign(aux,request);
    delete aux._id;
    return vue.http.put(url + '/' + id, aux);
}

export function deleteRequest(id) {
    return vue.http.delete(url + '/' + id);
}