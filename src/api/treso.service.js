import vue from "vue";
import config from "./config.json"

const url = config.url + "/api/tresos";

export function getAllTresos() {
    return vue.http.get(url);
}

export function getTresoById(id) {
    return vue.http.get(url + "/" + id);
}

export function addTreso(provider) {
    return vue.http.post(url, provider)
}

export function editTreso(id, provider) {
    let aux = new Object();
    Object.assign(aux,provider);
    delete aux._id;
    return vue.http.put(url + '/' + id, aux)
}

export function deleteTreso(id) {
    return vue.http.delete(url + '/' + id);
}