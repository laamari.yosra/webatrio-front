import * as axios from 'axios';

import config from "./config.json"
const BASE_URL = config.url;

function upload(formData) {
    const url = `${BASE_URL}/uploads`;
    return axios.post(url, formData)
}

export { upload }