import vue from "vue";
import config from "./config.json"

const url= config.url+"/api/users"; 

export function getAllUsers (){
    return vue.http.get(url);
}

export function editUser (id,user){
    let aux = new Object();
    Object.assign(aux,user);
    delete aux._id;
    return vue.http.put(url+'/'+id,aux)
}

export function deleteUser (id){
    return vue.http.delete(url+'/'+id);
}