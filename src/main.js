// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import BootstrapVue from  'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import moment from 'moment'
import store from './store'

Vue.use(BootstrapVue);
Vue.filter('formatdate',function(x){
  if(x){
    return moment(String(x)).format('DD/MM/YYYY')
  }
})

Vue.use(VueResource);
Vue.config.productionTip = false

import './theme.css'
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
