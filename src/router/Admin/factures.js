import Index from '@/components/Admin/Facture/Index.vue';
import Edit from '@/components/Admin/Facture/Edit.vue';
export default [
    {
        path: '',
        component: Index
    },
    {
        path: ':id/:index',
        component: Edit
    }
]