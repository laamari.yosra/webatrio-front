import Admin from '@/components/Admin/Admin.vue'
import UsersRouter from './users.js'
import Users from '@/components/shared/Users/Users.vue'
import AbsencesRouter from './absences.js'
import ProvidersRouter from './providers.js'
import Providers from '@/components/Admin/Providers/Providers.vue'
import Absences from '@/components/Admin/Absences/Absences.vue'
import TachesRouter from './taches.js'
import Taches from '@/components/shared/Taches/Taches.vue';
import Devis from '@/components/Admin/Devis.vue';
import ResourcesRouter from './resources.js'
import Resources from '@/components/Admin/Resources/Resources.vue';
import FacturesRouter from './factures.js'
import Factures from '@/components/Admin/Facture/Facture.vue';
import TresoRouter from './treso.js'
import Treso from '@/components/Admin/Treso/Treso.vue';


export default [
    {
        path: 'users',
        component: Users,
        children: UsersRouter
    },
    {
        path: 'providers',
        component: Providers,
        children: ProvidersRouter
    },
    {
        path: 'treso',
        component: Treso,
        children: TresoRouter
    },
    {
        path: 'absences',
        component: Absences,
        children: AbsencesRouter
    },
    {
        path: 'taches',
        component: Taches,
        children: TachesRouter
    },
    {
        path: 'devis',
        component: Devis
    },
    {
        path: 'resources',
        component: Resources,
        children: ResourcesRouter
    },
    {
        path: 'factures',
        component: Factures,
        children: FacturesRouter
    }
]