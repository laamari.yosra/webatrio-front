import New from '@/components/Admin/Providers/New.vue'
import Index from '@/components/Admin/Providers/Index.vue'
import Edit from '@/components/Admin/Providers/Edit.vue'

export default [
    {
        path : 'new',
        component : New
    },
    {
        path : '',
        component : Index
    },
    {
        path : ':id/edit',
        component : Edit
    }
  
]