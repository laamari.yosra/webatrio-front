import Index from '@/components/Admin/Resources/Index.vue';
import Edit from '@/components/Admin/Resources/Edit.vue';
export default [
    {
        path: '',
        component: Index
    },
    {
        path: ':id/:index',
        component: Edit
    }
]