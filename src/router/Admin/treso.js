import New from '@/components/Admin/Treso/New.vue'
import Index from '@/components/Admin/Treso/Index.vue'
import Edit from '@/components/Admin/Treso/Edit.vue'

export default [
    {
        path : 'new',
        component : New
    },
    {
        path : '',
        component : Index
    },
    {
        path : ':id/edit',
        component : Edit
    }
  
]