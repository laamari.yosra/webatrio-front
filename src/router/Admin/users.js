import New from '@/components/shared/Users/New.vue'
import Index from '@/components/shared/Users/Index.vue'
import Edit from '@/components/shared/Users/Edit.vue'

export default [
    {
        path : 'new',
        component : New
    },
    {
        path : '',
        component : Index
    },
    {
        path : ':id/edit',
        component : Edit
    }
  
]