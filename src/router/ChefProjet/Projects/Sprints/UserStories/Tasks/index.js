import Index from '@/components/ChefProjet/Projects/Sprints/UserStories/Tasks/Index.vue';
import New from '@/components/ChefProjet/Projects/Sprints/UserStories/Tasks/New.vue';
import Edit from '@/components/ChefProjet/Projects/Sprints/UserStories/Tasks/Edit.vue';

export default [
    {
        path:'',
        component:Index
    },
    {
        path: 'new',
        component: New
    },
    {
        path: ':taskId/edit',
        component: Edit
    }
]