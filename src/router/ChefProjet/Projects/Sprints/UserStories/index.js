import Index from '@/components/ChefProjet/Projects/Sprints/UserStories/Index.vue';
import New from '@/components/ChefProjet/Projects/Sprints/UserStories/New.vue';
import Edit from '@/components/ChefProjet/Projects/Sprints/UserStories/Edit.vue';

import Tasks from '@/components/ChefProjet/Projects/Sprints/UserStories/Tasks/Tasks.vue';
import TasksRouter from './Tasks';
export default [
    {
        path: '',
        component: Index
    },
    {
        path: 'new',
        component: New
    },
    {
        path: ':userStoryId/edit',
        component: Edit
    },
    {
        path: ':userStoryId/tasks',
        component: Tasks,
        children: TasksRouter
    },
]