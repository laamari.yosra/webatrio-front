import Index from '@/components/ChefProjet/Projects/Sprints/Index.vue';
import New from '@/components/ChefProjet/Projects/Sprints/New.vue';
import Edit from '@/components/ChefProjet/Projects/Sprints/Edit.vue';

import UserStoriesRouter from './UserStories';
import UserStories from '@/components/ChefProjet/Projects/Sprints/UserStories/UserStories.vue';

export default [
    {
        path: '',
        component: Index
    },
    {
        path: 'new',
        component: New
    },
    {
        path: ':sprintId/edit',
        component: Edit
    },
    {
        path: ':sprintId/userstories',
        component: UserStories,
        children : UserStoriesRouter
    }
]