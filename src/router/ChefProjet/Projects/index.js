import Index from '@/components/ChefProjet/Projects/Index.vue';
import New from '@/components/ChefProjet/Projects/New.vue';
import Edit from '@/components/ChefProjet/Projects/Edit.vue';
import Cahier from '@/components/ChefProjet/Projects/Cahier.vue';
import Facture from '@/components/ChefProjet/Projects/Facture.vue';
////Sprints
import Sprints from '@/components/ChefProjet/Projects/Sprints/Sprints.vue';
import SprintsRouter from './Sprints';
////Resources
import Resources from '@/components/ChefProjet/Projects/Resources/Resources.vue';
import ResourcesRouter from './resources.js';

export default [
    {
        path: '',
        component: Index
    },
    {
        path: 'new',
        component: New
    },
    {
        path: ':id/edit',
        component: Edit
    },
    {
        path: ':id/cahier',
        component: Cahier
    },
    {
        path: ':id/sprints',
        component: Sprints,
        children: SprintsRouter
    },
    {
        path: ':id/resources',
        component: Resources,
        children: ResourcesRouter
    },
    {
        path: ':id/facture',
        component: Facture
    }
]