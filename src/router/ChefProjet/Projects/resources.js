import Index from '@/components/ChefProjet/Projects/Resources/Index.vue';
import New from '@/components/ChefProjet/Projects/Resources/New.vue';
import Edit from '@/components/ChefProjet/Projects/Resources/Edit.vue';

export default [
    {
        path: '',
        component: Index
    },
    {
        path: 'new',
        component: New
    },
    {
        path: ':resourceId/edit',
        component: Edit
    }
]