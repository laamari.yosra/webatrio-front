import AbsenceRouter from './absences.js'
import Absences from '@/components/ChefProjet/Absences/Absences.vue'
import MyAbsenceRouter from './myabsences.js'
import MyAbsences from '@/components/ChefProjet/MyAbsences/Absences.vue'
import ProjectsRouter from './Projects'
import Projects from '@/components/ChefProjet/Projects/Projects.vue'
import RequestsRouter from './requests.js'
import Requests from '@/components/ChefProjet/Requests/Requests.vue'
import TachesRouter from './taches.js'
import Taches from '@/components/shared/Taches/Taches.vue';


export default [
    {
        path: 'absences',
        component: Absences,
        children: AbsenceRouter
    },
    {
        path: 'myabsences',
        component: MyAbsences,
        children: MyAbsenceRouter
    },
    {
        path: 'projects',
        component: Projects,
        children: ProjectsRouter
    },
    {
        path: 'requests',
        component: Requests,
        children: RequestsRouter
    },
    {
        path: 'taches',
        component: Taches,
        children: TachesRouter
    }
]