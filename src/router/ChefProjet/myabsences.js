import New from '@/components/ChefProjet/MyAbsences/New.vue';
import Edit from '@/components/ChefProjet/MyAbsences/Edit.vue';
import Index from '@/components/ChefProjet/MyAbsences/Index.vue';

export default [
    {
        path: 'new',
        component: New
    },
    {
        path: '',
        component: Index
    },
    {
        path: ':absenceId/edit',
        component: Edit
    }
]