import Index from '@/components/ChefProjet/Requests/Index.vue';
import New from '@/components/ChefProjet/Requests/New.vue';
import Edit from '@/components/ChefProjet/Requests/Edit.vue';


export default [
    {
        path: '',
        component: Index
    },
    {
        path: 'new',
        component: New
    },
    {
        path: ':requestId/edit',
        component: Edit
    }
]