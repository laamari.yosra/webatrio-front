import Index from '@/components/Commercial/Absences/Index.vue'
import New from '@/components/Commercial/Absences/New.vue'
import Edit from '@/components/Commercial/Absences/Edit.vue'
export default [
    {
        path : '',
        component : Index
    },
     {
        path : 'new',
        component : New
    },
    {
        path: ':absenceId/edit',
        component: Edit
    }
  
  
]