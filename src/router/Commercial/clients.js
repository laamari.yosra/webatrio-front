import Index from '@/components/Commercial/Clients/Index.vue';
import New from '@/components/Commercial/Clients/New.vue';
import Edit from '@/components/Commercial/Clients/Edit.vue';

export default [
    {
        path: '',
        component: Index
    },
    {
        path: 'new',
        component: New  
    },
    {
        path: ':id/edit',
        component: Edit  
    }

]