import Index from '@/components/Commercial/Companies/Index.vue';
import New from '@/components/Commercial/Companies/New.vue';
import Edit from '@/components/Commercial/Companies/Edit.vue';

export default [
    {
        path: '',
        component: Index
    },
    {
        path: 'new',
        component: New  
    },
    {
        path: ':id/edit',
        component: Edit  
    }

]