import Index from '@/components/Commercial/Factures/Index.vue'
import Edit from '@/components/Commercial/Factures/Edit.vue'
export default [
    {
        path: '',
        component: Index
    },
    {
        path: ':id',
        component: Edit
    }
]