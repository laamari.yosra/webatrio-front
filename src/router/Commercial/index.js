import ClientsRouter from './clients.js'
import CompaniesRouter from './companies.js'
import AbsencesRouter from './absences.js'
import Clients from '@/components/Commercial/Clients/Clients.vue'
import Companies from '@/components/Commercial/Companies/Companies.vue'
import Absences from '@/components/Commercial/Absences/Absences.vue'
import DevisRouter from './devis.js'
import Devis from '@/components/Commercial/Devis/Devis.vue'
import TachesRouter from './taches.js'
import Taches from '@/components/shared/Taches/Taches.vue';
import FacturesRouter from './factures.js'
import Factures from '@/components/Commercial/Factures/Factures.vue';

export default [
    {
        path: 'clients',
        component: Clients,
        children: ClientsRouter
    },
    {
        path: 'companies',
        component: Companies,
        children: CompaniesRouter
    },
    {
        path: 'absences',
        component: Absences,
        children: AbsencesRouter
    },
    {
        path: 'devis',
        component: Devis,
        children: DevisRouter
    },
    {
        path: 'factures',
        component: Factures,
        children: FacturesRouter
    },
    {
        path: 'taches',
        component: Taches,
        children: TachesRouter
    }
]