import New from '@/components/Developer/Absences/New.vue';
import Edit from '@/components/Developer/Absences/Edit.vue';
import Index from '@/components/Developer/Absences/Index.vue';

export default [
    {
        path: 'new',
        component: New
    },
    {
        path: '',
        component: Index
    },
    {
        path: ':absenceId/edit',
        component: Edit
    }
]