import AbsenceRouter from './absences.js'
import Absences from '@/components/Developer/Absences/Absences.vue';
import TachesRouter from './taches.js'
import Taches from '@/components/shared/Taches/Taches.vue';
import Developer from '@/components/Developer/Developer.vue';


export default [
    {
        path: 'absences',
        component: Absences,
        children: AbsenceRouter
    },
    {
        path: 'taches',
        component: Taches,
        children: TachesRouter
    }
]