import AbsenceRouter from './absences.js'
import Absences from '@/components/Rh/Absences/Absences.vue'
import UsersRouter from './users.js'
import Users from '@/components/shared/Users/Users.vue'
import RequestsRouter from './requests.js'
import Requests from '@/components/Rh/Requests/Requests.vue'
import Rh from '@/components/Rh/Rh.vue'
import TachesRouter from './taches.js'
import Taches from '@/components/shared/Taches/Taches.vue';

export default [
    {
        path: 'absences',
        component: Absences,
        children: AbsenceRouter
    },
    {
        path: 'users',
        component: Users,
        children: UsersRouter
    },
    {
        path: 'requests',
        component: Requests,
        children: RequestsRouter
    },
    {
        path: 'taches',
        component: Taches,
        children: TachesRouter
    }
]