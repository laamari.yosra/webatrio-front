import Index from '@/components/shared/Taches/Index.vue';
import New from '@/components/shared/Taches/New.vue';
import Edit from '@/components/shared/Taches/Edit.vue';

export default [
    {
        path: '',
        component: Index
    },
    {
        path: 'new',
        component: New
    },
    {
        path: ':eventId/edit',
        component: Edit
    }
]