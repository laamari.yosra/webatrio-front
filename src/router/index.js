import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import Login from '@/components/login.vue'

import AdminRouter from './Admin'
import DeveloperRouter from './Developer'
import CommercialRouter from './Commercial'
import ChefProjetRouter from './ChefProjet'
import RhRouter from './Rh'

import Admin from '@/components/Admin/Admin.vue'
import ChefProjet from '@/components/ChefProjet/ChefProjet.vue'
import Commercial from '@/components/Commercial/Commercial.vue'
import Developer from '@/components/Developer/Developer.vue'
import Rh from '@/components/Rh/Rh.vue'



Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/admin',
      component : Admin,
      children : AdminRouter
    },
    {
      path: '/rh',
      name: 'Rh',
      component: Rh,
      children : RhRouter
    },
    {
      path: '/chefProjet',
      name: 'ChefProjet',
      component:ChefProjet,
      children: ChefProjetRouter
    },
    {
      path: '/commercial',
      name: 'Commercial',
      component: Commercial,
      children : CommercialRouter
    },
    {
      path: '/developer',
      name: 'Developer',
      component: Developer,
      children : DeveloperRouter
    }
  ]
})
