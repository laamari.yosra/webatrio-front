import { getAllClients, getClientById } from '@/api/clients.service.js';
export default {
    state: {
        clients: [],
        client: {}
    },
    getters: {
        getAllClients(state) {
            return state.clients;
        },
        getCurrentClient(state) {
            return state.client;
        }
    },
    mutations: {
        setAllClients(state, clients) {
            state.clients = clients;
        },
        setCurrentClient(state, client) {
            state.client = client;
        },
        setClientById(state, client) {
            state.clients.forEach(function (c, i) {
                if (c._id === client._id) {
                    state.clients.splice(i, 1);
                }
            }, this);
            state.clients.push(client);
        }
    },
    actions: {
        setAllClients(context) {
            return new Promise((resolve, reject) => {
                getAllClients().then((response) => {
                    let clients = response.body.data;
                    context.dispatch('setAllCompanies').then((companies) => {
                        clients.forEach((c) => {
                            for (let i = 0; i < companies.length; i++) {
                                if (companies[i]._id === c.company._id) {
                                    c.company = companies[i];
                                    break;
                                }
                            }
                        });
                        context.commit('setAllClients', clients);
                        resolve(clients);
                    });
                }).catch(err => reject(err));
            });
        },
        setCurrentClient(context, client) {
            context.commit('setCurrentClient', client);
        },
        setClientById(context, id) {
            return new Promise((resolve, reject) => {
                getClientById(id).then((response) => {
                    let client = response.body.data;
                    context.dispatch('setCompanyById',client.company._id).then((company) => {
                        client.company = company;
                    });
                    context.commit('setCurrentClient', client);
                    context.commit('setClientById', client);
                    resolve(client);
                }).catch(err => reject(err));
            });
        }
    }
}