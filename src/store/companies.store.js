import { getAllCompanies, getCompanyById } from '@/api/companies.service.js';
export default {
    state: {
        companies: [],
        company: {}
    },
    getters: {
        getAllCompanies(state) {
            return state.companies;
        },
        getCurrentCompany(state) {
            return state.company;
        }
    },
    mutations: {
        setAllCompanies(state, companies) {
            state.companies = companies;
        },
        setCurrentCompany(state, company) {
            state.company = company;
        },
        setCompanyById(state, company) {
            state.companies.forEach(function (c, i) {
                if (c._id === company._id) {
                    state.companies.splice(i, 1);
                }
            }, this);
            state.companies.push(company);
        }
    },
    actions: {
        setAllCompanies(context) {
            return new Promise((resolve, reject) => {
                getAllCompanies().then((response) => {
                    let companies = response.body.data;
                    context.commit('setAllCompanies', companies);
                    resolve(companies);
                }).catch(err => reject(err));
            });
        },
        setCurrentCompany(context, company) {
            context.commit('setCurrentCompanies', company);
        },
        setCompanyById(context, id) {
            return new Promise((resolve, reject) => {
                getCompanyById(id).then((response) => {
                    let company = response.body.data;
                    context.commit('setCurrentCompany', company);
                    context.commit('setCompanyById', company);
                    resolve(company);
                }).catch(err => reject(err));
            });
        }
    }
}