import { getAllEvents, getEventById } from '@/api/events.service.js';
export default {
    state: {
        events: [],
        event: {}
    },
    getters: {
        getAllEvents(state) {
            return state.events;
        },
        getCurrentEvent(state) {
            return state.event;
        }
    },
    mutations: {
        setAllEvents(state, events) {
            state.events = events;
        },
        setCurrentEvent(state, event) {
            state.event = event;
        },
        setEventById(state, event) {
            state.events.forEach(function (c, i) {
                if (c._id === event._id) {
                    state.events.splice(i, 1);
                }
            }, this);
            state.events.push(event);
        }
    },
    actions: {
        setAllEvents(context) {
            return new Promise((resolve, reject) => {
                getAllEvents().then((response) => {
                    let events = response.body.data;
                    context.commit('setAllEvents', events);
                    resolve(events);
                }).catch(err => reject(err));
            });
        },
        setCurrentEvent(context, event) {
            context.commit('setCurrentEvents', event);
        },
        setEventById(context, id) {
            return new Promise((resolve, reject) => {
                getEventById(id).then((response) => {
                    let event = response.body.data;
                    context.commit('setCurrentEvent', event);
                    context.commit('setEventById', event);
                    resolve(event);
                }).catch(err => reject(err));
            });
        }
    }
}