import Vue from 'vue';
import Vuex from 'vuex';
import Users from './users.store';
import Clients from './clients.store.js';
import Companies from './companies.store.js';
import Providers from './providers.store.js';
import Projects from './projects.store.js';
import Events from './events.store.js';
import Requests from './requests.store.js';
import Tresos from './tresos.store.js';

Vue.use(Vuex);
export default new Vuex.Store({
    modules: {
        Users,
        Clients,
        Companies,
        Providers,
        Projects,
        Events,
        Requests,
        Tresos
    }
});