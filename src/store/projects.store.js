import { getAllProjects, getProjectById } from '@/api/projects.service.js';
export default {
    state: {
        projects: [],
        project: {}
    },
    getters: {
        getAllProjects(state) {
            return state.projects;
        },
        getCurrentProject(state) {
            return state.project;
        },
        getAllDevis(state) {
            let ret = new Array();
            state.projects.forEach((p) => {
                if (p.facture.type === "Devi") {
                    ret.push(p);
                }
            });
            return ret;
        },
        getAllFactures(state) {
            let ret = new Array();
            state.projects.forEach((p) => {
                if (p.facture.type === "Facture") {
                    ret.push(p);
                }
            });
            return ret;
        },
        getAllResources(state){
            let resources = new Array();
            let projects = state.projects;
            if (projects && projects.length > 0) {
                projects.forEach(function(p) {
                    if (p.resources && p.resources.length > 0) {
                        p.resources.forEach((r,i)=>{
                            r.pId = p._id;
                            r.pIndex = i;
                            resources.push(r);
                        });
                    }
                });
            }
            return resources;
        }
    },
    mutations: {
        setAllProjects(state, projects) {
            state.projects = projects;
        },
        setCurrentProject(state, project) {
            state.project = project;
        },
        setProjectById(state, project) {
            state.projects.forEach(function (c, i) {
                if (c._id === project._id) {
                    state.projects.splice(i, 1);
                }
            }, this);
            state.projects.push(project);
        }
    },
    actions: {
        setAllProjects(context) {
            return new Promise((resolve, reject) => {
                getAllProjects().then((response) => {
                    let projects = response.body.data;
                    context.dispatch('getAllUsers').then((users) => {
                        projects.forEach((p) => {
                            if (!p.technical) {
                                p.technical = new Object();
                            } else {
                                for (let i = 0; i < users.length; i++) {
                                    if (users[i]._id === p.technical._id) {
                                        p.technical = users[i];
                                        break;
                                    }
                                }
                            }
                            if (!p.commercial) {
                                p.commercial = new Object();
                            } else {
                                for (let i = 0; i < users.length; i++) {
                                    if (users[i]._id === p.commercial._id) {
                                        p.commercial = users[i];
                                        break;
                                    }
                                }
                            }
                            context.dispatch('setAllClients').then((clients) => {
                                if (p.facture) {
                                    if (p.facture.client) {
                                        for (let i = 0; i < clients.length; i++) {
                                            if (clients[i]._id === p.facture.client) {
                                                p.facture.client = clients[i];
                                            }
                                        }
                                    }
                                }
                                context.commit('setAllProjects', projects);
                                resolve(projects);
                            });
                        });
                    });
                }).catch(err => reject(err));
            });
        },
        setCurrentProject(context, project) {
            context.commit('setCurrentProjects', project);
        },
        setProjectById(context, id) {
            return new Promise((resolve, reject) => {
                getProjectById(id).then((response) => {
                    let p = response.body.data;
                    context.dispatch('getAllUsers').then((users) => {
                        if (!p.technical) {
                            p.technical = new Object();
                        } else {
                            for (let i = 0; i < users.length; i++) {
                                if (users[i]._id === p.technical._id) {
                                    p.technical = users[i];
                                    break;
                                }
                            }
                        }
                        if (!p.commercial) {
                            p.commercial = new Object();
                        } else {
                            for (let i = 0; i < users.length; i++) {
                                if (users[i]._id === p.commercial._id) {
                                    p.commercial = users[i];
                                    break;
                                }
                            }
                        }
                        context.commit('setCurrentProject', p);
                        context.commit('setProjectById', p);
                        resolve(p);
                    });
                }).catch(err => reject(err));
            });
        }
    }
}