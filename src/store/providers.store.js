import { getAllProviders, getProviderById } from '@/api/providers.service.js';
export default {
    state: {
        providers: [],
        provider: {}
    },
    getters: {
        getAllProviders(state) {
            return state.providers;
        },
        getCurrentProvider(state) {
            return state.provider;
        }
    },
    mutations: {
        setAllProviders(state, providers) {
            state.providers = providers;
        },
        setCurrentProvider(state, provider) {
            state.provider = provider;
        },
        setProviderById(state, provider) {
            state.providers.forEach(function (c, i) {
                if (c._id === provider._id) {
                    state.providers.splice(i, 1);
                }
            }, this);
            state.providers.push(provider);
        }
    },
    actions: {
        setAllProviders(context) {
            return new Promise((resolve, reject) => {
                getAllProviders().then((response) => {
                    let providers = response.body.data;
                    context.dispatch('setAllCompanies').then((companies) => {
                        providers.forEach((p) => {
                            if (!providers.company) {
                                providers.company = new Object();
                            }
                            for (let i = 0; i < companies.length; i++) {
                                if (companies[i]._id === p.company._id) {
                                    p.company = companies[i];
                                    break;
                                }
                            }
                        });
                        context.commit('setAllProviders', providers);
                        resolve(providers);
                    });
                }).catch(err => reject(err));
            });
        },
        setCurrentProvider(context, provider) {
            context.commit('setCurrentProviders', provider);
        },
        setProviderById(context, id) {
            return new Promise((resolve, reject) => {
                getProviderById(id).then((response) => {
                    let provider = response.body.data;
                    context.dispatch('setAllCompanies').then((companies) => {
                        for (let i = 0; i < companies.length; i++) {
                            if (companies[i]._id === provider.company._id) {
                                provider.company = companies[i];
                                break;
                            }
                        }
                        context.commit('setCurrentProvider', provider);
                        context.commit('setProviderById', provider);
                        resolve(provider);
                    });
                }).catch(err => reject(err));
            });
        }
    }
}