import { getAllRequests, getRequestById } from '@/api/requests.service.js';
export default {
    state: {
        requests: [],
        request: {}
    },
    getters: {
        getAllRequests(state) {
            return state.requests;
        },
        getCurrentRequest(state) {
            return state.request;
        }
    },
    mutations: {
        setAllRequests(state, requests) {
            state.requests = requests;
        },
        setCurrentRequest(state, request) {
            state.request = request;
        },
        setRequestById(state, request) {
            state.requests.forEach(function (c, i) {
                if (c._id === request._id) {
                    state.requests.splice(i, 1);
                }
            }, this);
            state.requests.push(request);
        }
    },
    actions: {
        setAllRequests(context) {
            return new Promise((resolve, reject) => {
                getAllRequests().then((response) => {
                    let requests = response.body.data;
                    context.commit('setAllRequests', requests);
                    resolve(requests);
                }).catch(err => reject(err));
            });
        },
        setCurrentRequest(context, request) {
            context.commit('setCurrentRequests', request);
        },
        setRequestById(context, id) {
            return new Promise((resolve, reject) => {
                getRequestById(id).then((response) => {
                    let request = response.body.data;
                    context.commit('setCurrentRequest', request);
                    context.commit('setRequestById', request);
                    resolve(request);
                }).catch(err => reject(err));
            });
        }
    }
}