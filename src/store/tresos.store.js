import { getAllTresos, getTresoById } from '@/api/treso.service.js';
export default {
    state: {
        tresos: [],
        treso: {}
    },
    getters: {
        getAllTresos(state) {
            return state.tresos;
        },
        getCurrentTreso(state) {
            return state.treso;
        }
    },
    mutations: {
        setAllTresos(state, tresos) {
            state.tresos = tresos;
        },
        setCurrentTreso(state, treso) {
            state.treso = treso;
        },
        setTresoById(state, treso) {
            state.tresos.forEach(function (c, i) {
                if (c._id === treso._id) {
                    state.tresos.splice(i, 1);
                }
            }, this);
            state.tresos.push(treso);
        }
    },
    actions: {
        setAllTresos(context) {
            return new Promise((resolve, reject) => {
                getAllTresos().then((response) => {
                    let tresos = response.body.data;
                    context.commit('setAllTresos', tresos);
                    resolve(tresos);
                }).catch(err => reject(err));
            });
        },
        setCurrentTreso(context, treso) {
            context.commit('setCurrentTresos', treso);
        },
        setTresoById(context, id) {
            return new Promise((resolve, reject) => {
                getTresoById(id).then((response) => {
                    let treso = response.body.data;
                    context.commit('setCurrentTreso', treso);
                    context.commit('setTresoById', treso);
                    resolve(treso);
                }).catch(err => reject(err));
            });
        }
    }
}