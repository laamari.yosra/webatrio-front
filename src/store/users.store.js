import { getAllUsers } from '@/api/users.service.js';
export default {
    state: {
        user: {

        },
        users: [

        ]
    },
    mutations: {
        setCurrentUser: function (state, user) {
            state.user = user;
        },
        getAllUsers: function (state, users) {
            state.users = users; 
        }
    },

   
    actions: {
        setCurrentUser(context, user) {
            context.commit('setCurrentUser', user);
        },
        getAllUsers(context) {
            return new Promise((resolve, reject) => {
                getAllUsers().then((response) => {
                    let users = response.body.data;
                    context.commit('getAllUsers', users);
                    resolve(users);
                }).catch((err) => {
                    reject(err);
                })
            })
        }
    },
    getters: {
        getCurrentUser(state) {
            return state.user;
        },
        AllUsers (state){
            return state.users;
        },
        getAllDevelopers(state){
            let devs = new Array();
            state.users.forEach((u)=>{
                if(u.role === "développeur"){
                    devs.push(u);
                }
            });
            return devs;
        },
        getAllCommercials(state){
            let devs = new Array();
            state.users.forEach((u)=>{
                if(u.role === "commercial"){
                    devs.push(u);
                }
            });
            return devs;
        }

    }
}